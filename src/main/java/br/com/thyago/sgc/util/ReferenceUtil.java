package br.com.thyago.sgc.util;

import org.omnifaces.config.BeanManager;

public final class ReferenceUtil {
	
	private ReferenceUtil() {
	}
	
	public static <T> T getReference(Class<T> clazz) {
		return BeanManager.INSTANCE.getReference(clazz);
	}

}
