package br.com.thyago.sgc.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_aluno")
public class Aluno implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "nr_cpf_aluno", unique= true, length = 11, nullable = false)
	private String cpf;
	
	@Column(name = "nm_aluno", length = 256, nullable = false)
	private String nome;
	
	@Column(name = "ds_endereco", length = 512, nullable = false)
	private String endereco;
	
	@Column(name = "sg_estado", length = 2, nullable = false)
	private String estado;
	
	@Column(name = "nr_telefone", length = 16, nullable = false)
	private String telefone;
	
	@Column(name = "ds_email", length = 128, nullable = false)
	private String email;
	
	@Column(name = "ds_senha", length = 256, nullable = false)
	private String senha;

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
}
