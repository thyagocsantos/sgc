package br.com.thyago.sgc.producer;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.omnifaces.cdi.ViewScoped;

import br.com.thyago.sgc.qualifier.RequestEntityManager;

@Named
public class EntityManagerProducer implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final ThreadLocal<EntityManager> ENTITY_MANAGER_LOCAL = new ThreadLocal<>();
	private static final ThreadLocal<EntityManager> ENTITY_MANAGER_REQUEST = new ThreadLocal<>();
	
	@PersistenceUnit(unitName = "sgcPU")
	private EntityManagerFactory entityFactory;
	
	@PersistenceUnit(unitName = "sgcPU")
	private EntityManagerFactory entityRequestFactory;
	
	@Produces
	@ViewScoped
	@Default
	public EntityManager createEntityFactory() {
		return createEntityManagerLocal();
	}
	
	@Produces
	@RequestScoped
	@RequestEntityManager
	public EntityManager createEntityRequestFactory() {
		return createEntityManagerRequest();
	}
	
	public void closeEntityManager(@Disposes EntityManager entityManager) {
		if (entityManager.isOpen()) {
			entityManager.close();
		}
		ENTITY_MANAGER_LOCAL.set(null);
	}
	
	public void closeEntityManagerRequest(@Disposes @RequestEntityManager EntityManager entityManager) {
		if (entityManager.isOpen()) {
			entityManager.close();
		}
		ENTITY_MANAGER_REQUEST.set(null);
	}
	
	private EntityManager createEntityManagerLocal() {
		EntityManager entityManager = ENTITY_MANAGER_LOCAL.get();
		if (entityManager == null) {
			entityManager = entityFactory.createEntityManager();
			entityManager.setProperty("org.hibernate.flushMode", "MANUAL");
			ENTITY_MANAGER_LOCAL.set(entityManager);
		} 
		return entityManager;
	}
	
	private EntityManager createEntityManagerRequest() {
		EntityManager entityManager = ENTITY_MANAGER_REQUEST.get();
		if (entityManager == null) {
			entityManager = entityRequestFactory.createEntityManager();
			entityManager.setProperty("org.hibernate.flushMode", "MANUAL");
			ENTITY_MANAGER_REQUEST.set(entityManager);
		} 
		return entityManager;
	}
	
}
