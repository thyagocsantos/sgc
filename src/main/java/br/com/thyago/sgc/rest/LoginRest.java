package br.com.thyago.sgc.rest;

import java.io.Serializable;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.gson.Gson;

import br.com.thyago.sgc.dao.AlunoDAO;
import br.com.thyago.sgc.model.Aluno;
import br.com.thyago.sgc.util.ReferenceUtil;

@Path(LoginRest.PATH)
@Produces("application/json;charset=iso-8859-1")
public class LoginRest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final String PATH = "/rest/login";
	
	private AlunoDAO alunoDAO = ReferenceUtil.getReference(AlunoDAO.class);
	
	@GET
	@Path("/logar")
	public Response logar(@QueryParam("login") String login, 
			  @QueryParam("password") String password) {
		Aluno aluno = alunoDAO.retrieveAlunoByCpfSenha(login, password);
		if (aluno != null) {
			String json = new Gson().toJson(aluno);
			return Response.ok(json).build();
		} else {
			return Response.status(Status.UNAUTHORIZED).build();
		}
	}

}
