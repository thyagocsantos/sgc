package br.com.thyago.sgc.rest.interceptor;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import br.com.thyago.sgc.rest.LoginRest;

public class RestInterceptorApp extends Application{
	
	private Set<Object> singletons = new HashSet<>();
	private Set<Class<?>> empty = new HashSet<>();
	
	public RestInterceptorApp() {
		this.singletons.add(new LoginRest());
	}
	
	@Override
	public Set<Class<?>> getClasses() {
		return this.empty;
	}
	
	@Override
	public Set<Object> getSingletons() {
		return this.singletons;
	}
}
