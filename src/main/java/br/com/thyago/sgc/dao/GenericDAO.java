package br.com.thyago.sgc.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.ContextNotActiveException;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.Query;

import br.com.thyago.sgc.qualifier.RequestEntityManager;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class GenericDAO implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String QUERY_CACHE_HINT = "org.hibernate.cacheable";
	
	@Inject
	protected EntityManager entityManager;
	
	@Inject
	@RequestEntityManager
	protected EntityManager entityManagerRequest;
	
	public <E> E find(Class<E> clazz, Object id) {
		if (id == null) {
			return null;
		}
		return entityManager.find(clazz, id);
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public <T> T persist(T obj) {
		entityManager.persist(obj);
		flush();
		return obj;
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public <T> void remove(T obj){
		entityManager.remove(obj);
		flush();
	}	
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void removeVarios(Object... parametros) {
		for (Object objeto : parametros){
			entityManager.remove(objeto);
		}
		flush();
	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public <T> T update(T obj){
		T res = entityManager.merge(obj);
		flush();
		return res;
	}	
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void persistVarios(Object... parametros){
		for (Object objeto : parametros){
			entityManager.persist(objeto);
		}
		flush();
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public <T> T createPessimisticLock(Class<T> clazz, Object id){
		return entityManager.find(clazz, id, LockModeType.PESSIMISTIC_READ);
	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void updateVarios(Object... parametros){
		for (Object objeto : parametros){
			entityManager.merge(objeto);
		}
		flush();
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void flush() {
		entityManager.flush();
	}
	
	public void enableQueryCache(Query query){
		query.setHint(QUERY_CACHE_HINT, "true");
	}
	
	@SuppressWarnings("unchecked")	
	public <T> T getSingleResult(Query query){
		query.setMaxResults(1);
		List<T> lista = query.getResultList();
		if (lista != null && !lista.isEmpty()){
			return lista.get(0);
		} else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T> Set<T> getResultSetList(Query query) {
		Set<T> setList = new HashSet<>(query.getResultList());
		return setList;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getResultList(Query query){
		List<T> lista = query.getResultList();
		return lista;
	}
	
	public <E> E getReference(Class<E> clazz, Object id){
		return entityManager.getReference(clazz, id);
	}	
	
	public boolean isManaged(Object entity){
		return entityManager.contains(entity);
	}
	
	public void refresh(Object entity){
		entityManager.refresh(entity);
	}
	
	public List<Map<String, Object>> getResultListAsMapList(List<? extends Object[]> lista, String[] alias){
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>(lista.size());
		for (Object[] values : lista) {
			Map<String, Object> map = new HashMap<String, Object>(values.length);
			for (int j = 0; j < alias.length ; j++) {
				map.put(alias[j], values[j]);
			}
			mapList.add(map);
		}
		return mapList;
	}
	
	public Map<String, Object> getResultAsMap(Object[] result, String[] alias){
		Map<String, Object> map = new HashMap<String, Object>(result.length);
		for (int j = 0; j < alias.length ; j++) {
			map.put(alias[j], result[j]);
		}
		return map;
	}
	
	public EntityManager getEntityManager() {
		try {
			if (entityManager.isOpen()) {
				return entityManager;
			}
			return entityManagerRequest;			
		} catch (ContextNotActiveException e) {
			return entityManagerRequest;
		}
	}


}
