package br.com.thyago.sgc.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import br.com.thyago.sgc.model.Aluno;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class AlunoDAO extends GenericDAO {

	private static final long serialVersionUID = 1L;
	
	public Aluno retrieveAlunoByCpfSenha(String cpf, String senha) {
		String hql = "select o from Aluno o " + 
				"where o.cpf = :cpf and " +
				"o.senha = :senha";
		Query query = getEntityManager().createQuery(hql);
		query.setParameter("cpf", cpf);
		query.setParameter("senha", senha);
		return getSingleResult(query);
	}
}
